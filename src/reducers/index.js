/*-------------------------------------------------------------------------------------------*/
import { combineReducers } from "redux";
import { default as appReducer } from "./appReducer";

const rootReducer = combineReducers({
  app: appReducer
});

export default rootReducer;
/*-------------------------------------------------------------------------------------------*/
