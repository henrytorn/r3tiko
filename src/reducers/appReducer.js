import { actions } from "../actions/appActions";

const initialStore = {
  isLogged: false,
  username: null
};

export default (store = initialStore, { type, payload }) => {
  switch (type) {
    case actions.LOGIN.FULFILLED:
      return {
        ...store,
        isLogged: true,
        username: payload.data.username
      };

    case actions.LOGOUT.NAME:
      return {
        ...store,
        isLogged: false
      };

    default:
      return store;
  }
};
