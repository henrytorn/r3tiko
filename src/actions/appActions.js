import { createActionPointers } from "./actionTools";
import axios from "axios";
export const actions = createActionPointers([
  "LOGIN",
  "CHANGE_USERNAME",
  "LOGOUT",
  "LOGIN_FAILURE"
]);

export const login = data => ({
  type: actions.LOGIN.NAME,
  //TÄHÄN LOGIN-RAJAPINTA
  payload: axios.post("http://localhost:8080/login", data)
});
export const logout = data => ({
  type: actions.LOGOUT.NAME
});

// export const changeUsername = name => ({
//   type: actions.CHANGE_USERNAME.NAME,
//   payload: name
// });
