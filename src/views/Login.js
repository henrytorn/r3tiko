import React from "react";
import { Connect } from "../actions";
import { Form, Button, Jumbotron } from "react-bootstrap";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      errors: {}
    };
  }

  changeText = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <div>
        <Jumbotron>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Käyttäjänimesi</Form.Label>
              <Form.Control
                value={this.state.username}
                onChange={this.changeText}
                type="text"
                placeholder="Syötä käyttäjänimesi"
                name="username"
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Salasana</Form.Label>
              <Form.Control
                value={this.state.password}
                onChange={this.changeText}
                type="password"
                placeholder="Password"
                name="password"
              />
            </Form.Group>
            <Button
              onClick={() =>
                this.props.login({
                  username: this.state.username,
                  password: this.state.password
                })
              }
            >
              Login
            </Button>
          </Form>
        </Jumbotron>
      </div>
    );
  }
}

export default Connect(Login);
