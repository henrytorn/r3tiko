import React from "react";
import Login from "../views/Login";
import Home from "../views/Home";
import { Connect } from "../actions";

class ScreenContainer extends React.Component {
  render() {
    if (!this.props.app.isLogged) {
      return (
        <div>
          <Login login={this.login} />
        </div>
      );
    } else {
      return (
        <div>
          <Home />
        </div>
      );
    }
  }
}

export default Connect(ScreenContainer);
