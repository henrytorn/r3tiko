import React from "react";
import { Connect } from "../actions";
import Navigationbar from "./Navigationbar";
import { Jumbotron, Table, Button } from "react-bootstrap";
import axios from 'axios';
import moment from "moment";
import {Bar} from 'react-chartjs-2'

class Home extends React.Component {

  
  componentDidMount() {
    this.getMemory();
  }


  state  = {
      time: "",
      free: "",
      used: "",
      cached: "",
      active: "",
      total: "",

      memory: []
  }

   getMemory = () => {
    axios.get("http://localhost:8080/api/memory").then(res =>
      this.setState({
        memory: res.data
      })
    );
  };


  createMemory = () => {
    let post = {
      time: this.state.time,
      free: this.state.free,
      used: this.state.used,
      cached: this.state.cached,
      active: this.state.active,
      total: this.state.total
    };
    axios
      .post("http://localhost:8080/api/memory", post)
      .then(res => this.getMemory());
  };


  render() {
    return (
      <div>
        <Navigationbar />
        <Jumbotron>
        

        <Table striped bordered hover>
                    <thead>
                      <tr>
                        <th>date</th>
                        <th>free</th>
                        <th>used</th>
                        <th>cached</th>
                        <th>active</th>
                        <th>total</th>
                      </tr>
                    </thead>
                    <tbody>
                    {this.state.memory.map(mem => (
                    <tr>
                      <td>
                            {moment(mem.time).format("DD.MM.YYYY")}
                      </td>
                      <td>{mem.free}{"  bittiä"}</td>
                      <td>{mem.used}</td>
                      <td>{mem.cached}</td>
                      <td>{mem.active}</td>
                      <td>{mem.total}</td>
                    </tr>
                    ))}
                    </tbody>
                  </Table>
                  <Button onClick={this.createMemory} variant="primary">
                    Vie muisti
                  </Button>


                  <Bar
          data={{
            datasets:[
              {
                data: this.state.memory.map(item => item.active),
                label: "Active",
                backgroundColor:'green' 
              },
              {
                data: this.state.memory.map(item => item.used),
                label: "Used",
                backgroundColor:'red' 
              },
              {
                data: this.state.memory.map(item => item.free),
                label: "Free",
                backgroundColor:'blue' 
              },
              {
                data: this.state.memory.map(item => item.total),
                label: "Total",
                backgroundColor:'orange' 
              }
            ],
            labels:this.state.memory.map(item => moment(item.time).format("DD.MM.YYYY"))
          }}
          width={100}
          height={30}
          options={{ maintainAspectRatio: false }}
        />
        </Jumbotron>
        
      </div>
    );
  }
}

export default Connect(Home);
