import React from "react";
import { Connect } from "../actions";
import { Navbar, Button } from "react-bootstrap";

class Navigationbar extends React.Component {
  render() {
    return (
      <div>
        <Navbar>
          <Navbar.Brand>Tervetuloa {this.props.app.username}</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
              Signed in as: <strong>{this.props.app.username} </strong>
            </Navbar.Text>
            <Button variant="light" onClick={() => this.props.logout()}>
              Kirjaudu ulos
            </Button>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default Connect(Navigationbar);
