/* *******************************************************************************************************************************/

import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import reducers from "./reducers";
import promise from "redux-promise-middleware";
import { createLogger } from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";

const loggerMiddleWare = createLogger();

const middleWares = applyMiddleware(promise, thunk, loggerMiddleWare);

const store = createStore(reducers, composeWithDevTools(middleWares));

export default store;
/* *******************************************************************************************************************************/
