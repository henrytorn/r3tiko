import React from "react";
import "./App.css";
import store from "./store";
import { Provider } from "react-redux";
import { Connect } from "./actions";
import ScreenContainer from "./views/ScreenContainer";

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ScreenContainer />
      </Provider>
    );
  }
}

export default App;
